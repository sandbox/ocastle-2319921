
-- SUMMARY --

The bootstrap formatters module adds additional field formatters to convert fields into bootstrap elements.
E.g Multi-Image field into a Carousel

For a full description of the module, visit the project page:
  http://drupal.org/project/bootstrap_formatters

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/bootstrap_formatters


-- REQUIREMENTS --

This module does not include bootstrap as it just changes field output to suit bootstrap requirements.

* Bootstrap Theme - https://www.drupal.org/project/bootstrap

This module has no module requirements to run, but to be useful you may need to enable to correct field types. E.g. Image


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure field formatters in Structure » Content Types » Manage Display:

  - Select the format as the relevant bootstrap element.
  - Configure the field display settings to your requirements.


-- CONTACT --

Current maintainers:
* Oliver Castle (ocastle) - https://www.drupal.org/user/1798580

This project has been sponsored by:
* Fullbundle
  Creative Drupal agency providing Drupal sites. Visit http://www.fullbundle.com for more information.