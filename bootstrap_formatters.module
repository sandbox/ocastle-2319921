<?php

/**
 * @file
 * Adds Twitter Bootstrap Field Formatters
 */

/**
 * Implements hook_theme().
 */
function bootstrap_formatters_theme() {
    return array(
        'boostrap_carousel' => array(
            'variables' => array(
                'slides' => array(),
                'indicators' => TRUE,
                'controls' => TRUE,
                'image_style' => 'large',
                'attributes' => array(),
            ),
        )
    );
}

/**
 * Implements hook_field_formatter_info().
 */
function bootstrap_formatters_field_formatter_info() {
    return array(
        'bootstrap_carousel' => array(
            'label' => t('Bootstrap Carousel'),
            'field types' => array('image'),
            'settings' => array(
                'carousel_id' => 'carousel',
                'indicators' => TRUE,
                'controls' => TRUE,
                'image_style' => 'large',
                'nocarousel_on_oneimage' => FALSE
            )
        )
    );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function bootstrap_formatters_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {

    $display = $instance['display'][$view_mode];
    $settings = $display['settings'];

    //Initialize element
    $element = array();


    switch ($display['type']) {
        case 'bootstrap_carousel':

            //Display Indicators
            $element['carousel_id'] = array(
                '#type' => 'textfield',
                '#title' => t('Carousel ID'),
                '#description' => t('The Carousel HTML ID'),
                '#default_value' => $settings['carousel_id'],
            );
            $image_styles = image_style_options(FALSE);
            $element['image_style'] = array(
                '#title' => t('Image Style'),
                '#type' => 'select',
                '#default_value' => $settings['image_style'],
                '#options' => $image_styles,
            );
            $element['indicators'] = array(
                '#type' => 'checkbox',
                '#title' => t('Display Indicators'),
                '#description' => t('Whether or not to display bootstrap carousel indicators'),
                '#default_value' => $settings['indicators'],
            );
            $element['controls'] = array(
                '#type' => 'checkbox',
                '#title' => t('Display Controls'),
                '#description' => t('Whether or not to display bootstrap carousel controls'),
                '#default_value' => $settings['controls'],
            );
            $element['nocarousel_on_oneimage'] = array(
                '#type' => 'checkbox',
                '#title' => t('No carousel on a single image'),
                '#description' => t('This removes the carousel functionality when there is only one image'),
                '#default_value' => $settings['nocarousel_on_oneimage'],
            );
            break;
    }

    return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function bootstrap_formatters_field_formatter_settings_summary($field, $instance, $view_mode) {

    $display = $instance['display'][$view_mode];
    $settings = $display['settings'];

    $summary = array();

    switch ($display['type']) {
        case 'bootstrap_carousel':

            $summary[] = t('Carousel Image Style: @style', array(
                '@style' => $settings['image_style'],
            ));

            $summary[] = t('Display carousel indicators: @indicators', array(
                '@indicators' => ($settings['indicators']) ? 'yes' : 'no',
            ));

            $summary[] = t('Display carousel controls: @controls', array(
                '@controls' => ($settings['controls']) ? 'yes' : 'no',
            ));

            break;
    }
    return implode('<br />', $summary);
}

/**
 * Implements hook_field_formatter_view().
 */
function bootstrap_formatters_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {

    $element = array();
    $settings = $display['settings'];

    switch ($display['type']) {
        case 'bootstrap_carousel':

            $carousel_id = $settings['carousel_id'];
            $indicators = $settings['indicators'];
            $controls = $settings['controls'];
            $image_style = $settings['image_style'];
            $nocarousel_on_oneimage = $settings['nocarousel_on_oneimage'];

            $attributes = array('id' => $carousel_id);

            $slides = array();
            foreach ($items as $item) {
                $slides[] = array(
                    'path' => $item['uri'],
                    'alt' => $item['alt'],
                    'title' => $item['title']
                );
            }

            if ($nocarousel_on_oneimage && count($slides) === 1)
                $element[0] = array(
                    '#theme' => 'image_style',
                    '#style_name' => $image_style,
                    '#path' => $slides[0]['path'],
                    '#alt' => (isset($slides[0]['alt'])) ? $slides[0]['alt'] : NULL,
                    '#title' => (isset($slides[0]['title'])) ? $slides[0]['title'] : NULL
                );
            else
            $element[0]['#markup'] = theme('boostrap_carousel', array('slides' => $slides, 'indicators' => $indicators, 'controls' => $controls, 'image_style' => $image_style, 'attributes' => $attributes));

            break;
    }

    return $element;
}

function theme_boostrap_carousel($variables) {
    $slides = $variables['slides'];
    $indicators = $variables['indicators'];
    $controls = $variables['controls'];
    $image_style = $variables['image_style'];
    $attributes = $variables['attributes'];

    //Build Attributes
    //HTML ID
    $carousel_id = (isset($attributes['id'])) ? drupal_html_id($attributes['id']) : drupal_html_id('carousel');
    $attributes['id'] = $carousel_id;

    //HTML CLASSES
    if (!isset($attributes['class']))
        $attributes['class'] = array();

    if (!in_array('carousel', $attributes['class']))
        $attributes['class'][] = drupal_html_class('carousel');
    if (!in_array('slide', $attributes['class']))
        $attributes['class'][] = drupal_html_class('slide');

    //data-ride Attribute
    $attributes['data-ride'] = 'carousel';


    //Wrapper
    $element = array(
        '#type' => 'container',
        '#attributes' => $attributes,
    );

    if ($indicators && !empty($slides)) {
        $element['indicators'] = array(
            '#theme' => 'item_list',
            '#type' => 'ol',
            '#attributes' => array('class' => array('carousel-indicators')),
            '#items' => array()
        );

        for ($i = 0; $i < count($slides); $i++) {
            $element['indicators']['#items'][$i] = array(
                'data' => '',
                'data-target' => '#' . $carousel_id,
                'data-slide-to' => $i,
            );
            if ($i === 0)
                $element['indicators']['#items'][$i]['class'] = array('active');
        }
    }

    $element['slides'] = array(
        '#type' => 'container',
        '#attributes' => array(
            'class' => array('carousel-inner')
        )
    );
    if (!empty($slides)) {
        $first = TRUE;
        foreach ($slides as $key => $slide) {
            $element['slides'][$key] = array(
                '#type' => 'container',
                '#attributes' => array(
                    'class' => array('item')
                ),
                'slide' => array(
                    '#theme' => 'image_style',
                    '#style_name' => $image_style,
                    '#path' => $slide['path'],
                    '#alt' => (isset($slide['alt'])) ? $slide['alt'] : NULL,
                    '#title' => (isset($slide['title'])) ? $slide['title'] : NULL,
                )
            );
            if (isset($slide['title'])) {
                $element['slides'][$key]['caption'] = array(
                    '#type' => 'container',
                    '#attributes' => array(
                        'class' => array('carousel-caption')
                    ),
                    'content' => array(
                        '#markup' => $slide['title']
                    )
                );
            }
            if ($first) {
                $element['slides'][$key]['#attributes']['class'][] = 'active';
                $first = FALSE;
            }
        }
    }

    if ($controls) {
        $element['controls'] = array(
            array(
                '#theme' => 'link',
                '#text' => '<span class="glyphicon glyphicon-chevron-left"></span>',
                '#path' => NULL,
                '#options' => array(
                    'fragment' => $carousel_id,
                    'html' => TRUE,
                    'attributes' => array(
                        'class' => array('left', 'carousel-control'),
                        'role' => 'button',
                        'data-slide' => 'prev'
                    )
                )
            ),
            array(
                '#theme' => 'link',
                '#text' => '<span class="glyphicon glyphicon-chevron-right"></span>',
                '#path' => NULL,
                '#options' => array(
                    'fragment' => $carousel_id,
                    'html' => TRUE,
                    'attributes' => array(
                        'class' => array('right', 'carousel-control'),
                        'role' => 'button',
                        'data-slide' => 'next'
                    )
                )
            )
        );
    }
    return drupal_render($element);
}
